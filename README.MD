
I know: there are a lot of Dockerized **[duplicity](http://duplicity.nongnu.org/)** out there, but I was not lucky enough to find one working with swift 2.0 containers, so I made my own ;)

It's not a small package, I'm afraid, but the fastest way I managed to build a complete duplicity env was with an ubuntu.


## Restore example:

    $ docker run --rm -v local_restore_directory:/data \
      -e SWIFT_USERNAME="" \
      -e SWIFT_TENANTNAME="" \
      -e SWIFT_PASSWORD="" \
      -e SWIFT_AUTHURL="" \
      -e SWIFT_AUTHVERSION=2 \
      -e PASSPHRASE="" \
      antoniopayclick/duplicity:1.0 duplicity restore swift://CONTAINER_NAME ./data

(set your SWIFT variables, PASSPHRASE and CONTAINER_NAME)
